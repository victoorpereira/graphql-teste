package com.br.victor.graphqlteste.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.br.victor.graphqlteste.R
import kotlinx.android.synthetic.main.content_cachorros.view.*

/**
 * Created by Victor Pereira on 08/10/2018.
 * DevMaker Mobile Apps
 **/

class CachorroAdapter(val context: Context) : BaseAdapter<String,CachorroAdapter.ViewHolder>(context) {

    var onClick: (id: Int) -> Unit = {_ ->    }

    var onMenu: (idUsuario: Int, idComunidade: Int, idComentario: Int) -> Unit = {_,_,_ ->    }

    override fun onBindViewHolder(holder: CachorroAdapter.ViewHolder, position: Int) {

        with(holder){

            holder.nome.text = selectorList[position]

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = inflater.inflate(R.layout.content_cachorros, parent, false)
        val mvh = ViewHolder(v)
        return  mvh
    }

    inner class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview){

        val nome = itemView.nome

    }
}