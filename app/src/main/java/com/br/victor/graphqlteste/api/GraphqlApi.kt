package com.br.victor.graphqlteste.api

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import com.apollographql.apollo.ApolloClient
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.runOnUiThread
import java.util.concurrent.TimeUnit

/**
 * Created by Victor Pereira on 08/10/2018.
 * DevMaker Mobile Apps
 **/

interface GraphqlApi{

    companion object {

        val URL = "https://api.graph.cool/simple/v1/cjmz9qgrj2o0j015860e6t2ly"

        fun create(context: Context, show:Boolean=false, message:String="Aguarde..."): ApolloClient {

            val logInterceptor = HttpLoggingInterceptor()
            logInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val headintercepter = Interceptor { chain ->
                val request = chain.request().newBuilder()
//                        .addHeader("Content-Type", "application/json")

                var progressDialog: ProgressDialog? = null
                if(context is Activity && show){
                    context.runOnUiThread {
                        progressDialog = ProgressDialog(context)
                        progressDialog?.isIndeterminate = true
                        progressDialog?.setCanceledOnTouchOutside(false)
                        progressDialog?.setCancelable(false)
                        progressDialog?.setMessage(message)
                        progressDialog?.show()
                    }
                }

                try {
                    val response = chain.proceed(request.build())


                    context.runOnUiThread {
                        progressDialog?.dismiss()
                    }

                    when(response.code()){
                        200-> return@Interceptor response
                        201-> return@Interceptor response
                        400-> return@Interceptor response
                        401-> return@Interceptor response
                        404-> return@Interceptor response
                        500-> return@Interceptor response
                        else-> return@Interceptor response
                    }

                }catch (x:Throwable){
                    context.runOnUiThread {
                        progressDialog?.dismiss()
                    }
                    throw x
                }
            }

            val okHttpClient = OkHttpClient().newBuilder()
                    .connectTimeout(6000, TimeUnit.MILLISECONDS)
                    .readTimeout((1000 * 60).toLong(), TimeUnit.MILLISECONDS)
                    .writeTimeout((1000 * 60).toLong(), TimeUnit.MILLISECONDS)
                    .addInterceptor(logInterceptor)
                    .addInterceptor(headintercepter)
                    .build()

            val apollo = ApolloClient.builder()
                    .serverUrl(URL)
                    .okHttpClient(okHttpClient)
                    .build()

            return apollo

        }

    }

}