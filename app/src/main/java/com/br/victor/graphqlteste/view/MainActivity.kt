package com.br.victor.graphqlteste.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import br.com.andersonsoares.utils.showDialog
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.br.victor.graphqlteste.CadastrarUsuarioMutation
import com.br.victor.graphqlteste.R
import com.br.victor.graphqlteste.VerUsuarioQuery
import com.br.victor.graphqlteste.adapter.CachorroAdapter
import com.br.victor.graphqlteste.api.GraphqlApi
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var adapter: CachorroAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.title = "Grafiqueéli"
        toolbar.setNavigationIcon(R.drawable.ic_nav)

        pesquisar.setOnClickListener {
            getDados("cjmzhwf6x03of0115x7808j33")
        }

        cadastrar.setOnClickListener {
            if(cadastradoCl.visibility == View.VISIBLE){

                nomeEdit.text.clear()
                emailEdit.text.clear()
                idadeEdit.text.clear()

                cadastrar.text = "Cadastrar"
                cadastrarCl.visibility = View.VISIBLE
                cadastradoCl.visibility = View.GONE
            }else{
                if(!nomeEdit.text.toString().trim().isEmpty()){
                    if(!emailEdit.text.toString().trim().isEmpty()){
                        putCadastrarUsuario(nomeEdit.text.toString(), emailEdit.text.toString(), idadeEdit.text.toString())
                    }else{
                        showDialog("Email obrigatório")
                    }
                }else{
                    showDialog("Nome obrigatório")
                }
            }
        }

        adapter = CachorroAdapter(this@MainActivity)
        val llm =  LinearLayoutManager(this@MainActivity)
        llm.orientation = LinearLayoutManager.VERTICAL
        recyclerCachorros.layoutManager = llm
        recyclerCachorros.adapter = adapter
        recyclerCachorros.isNestedScrollingEnabled = false

    }

    private fun putCadastrarUsuario(nome: String, email: String, idade: String) {

        val idadeInt: Int?
        if(idade == ""){
            idadeInt = null
        }else{
            idadeInt = idade.toInt()
        }

        val cachorros: ArrayList<String>? = null

        GraphqlApi.create(this, true).mutate(
                CadastrarUsuarioMutation.builder()
                        .nome(nome)
                        .email(email)
                        .idade(idadeInt)
                        .cachorros(cachorros)
                        .build()).enqueue(object : ApolloCall.Callback<CadastrarUsuarioMutation.Data>(){
            override fun onResponse(response: Response<CadastrarUsuarioMutation.Data>) {
                runOnUiThread {
                    if(!response.hasErrors()){
                        response.data()?.let {
                            nome2.text = it.createUsuario()?.nome()
                            email2.text = it.createUsuario()?.email()
                            if(it.createUsuario()?.idade() != null){
                                idade2.text = it.createUsuario()?.idade().toString()
                                idade2.visibility = View.VISIBLE
                                textView6.visibility = View.VISIBLE
                            }else{
                                idade2.visibility = View.GONE
                                textView6.visibility = View.GONE
                            }
                        }
                        cadastrarCl.visibility = View.GONE
                        cadastrar.text = "Voltar"
                        cadastradoCl.visibility = View.VISIBLE
                    }else{
                        showDialog("Atenção!", response.errors().get(0).message().toString())
                    }
                }
            }

            override fun onFailure(e: ApolloException) {
                runOnUiThread {
//                    showDialog(e.message.toString())
                    showDialog(e.cause.toString())
//                    showDialog(e.localizedMessage)
                }
            }
        })

    }

    fun getDados(id: String){

        GraphqlApi.create(this, true).query(
                VerUsuarioQuery.builder()
                        .id(id)
                        .build()).enqueue(object : ApolloCall.Callback<VerUsuarioQuery.Data>() {
            override fun onResponse(response: Response<VerUsuarioQuery.Data>) {
                runOnUiThread {
                    if(!response.hasErrors()){
                        response.data()?.let {
                            nome.text = it.Usuario()?.nome()
                            email.text = it.Usuario()?.email()
                            if(it.Usuario()?.idade() != null){
                                idade.text = it.Usuario()?.idade().toString()
                                idade.visibility = View.VISIBLE
                                textView3.visibility = View.VISIBLE
                            }else{
                                idade.visibility = View.GONE
                                textView3.visibility = View.GONE
                            }
                            adapter?.replace(ArrayList(it.Usuario()?.cachorros()))
                        }
                    }else{
                        showDialog("Atenção!", response.errors().get(0).message().toString())
                    }
                }
            }

            override fun onFailure(e: ApolloException) {
                runOnUiThread {
//                    showDialog(e.message.toString())
                    showDialog(e.cause.toString())
//                    showDialog(e.localizedMessage)
                }
            }
        })

    }
}